import {Particle} from "./particle.js";

export class App {
	constructor (colors) {
		this.canvas = document.querySelector('#canvas1');
		this.ctx = this.canvas.getContext('2d');
		this.hue = 1;
		this.numberOfParticles = 50;
		this.colors = colors || 'white';

		window.addEventListener('resize', this.resize.bind(this), false);
		this.resize();
		this.animate();
	}
	resize () {
		this.clear();
		this.stageWidth = window.innerWidth;
		this.stageHeight = window.innerHeight;

		console.log(this.stageWidth / 7 )
		this.numberOfParticles = Math.floor(this.stageWidth / 7);

		this.canvas.width = this.stageWidth;
		this.canvas.height = this.stageHeight;

		this.init();
	}

	init () {
		this.particleArray = [];
		for (let i = 0; i < this.numberOfParticles; i++) {
			let x = Math.random() * this.canvas.width;
			let y = Math.random() * this.canvas.height;
			let size = (Math.random() * 20) + 15;
			// let color = `hsl(${this.hue}, 100%, 50%)`;
			let color = this.colors;

			let weight = (Math.random() * 10) + 2;
			this.particleArray.push(new Particle(x, y, size, color, weight));
			this.hue++;
		}
	}

	clear () {
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	animate () {
		requestAnimationFrame(this.animate.bind(this));
		//this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		for (let i = 0; i < this.particleArray.length; i++) {
			this.particleArray[i].update(this.ctx, this.canvas);
		}
	}
}