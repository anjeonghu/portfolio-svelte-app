export class Particle {
	constructor(x, y, size, color, weight) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.color = color;
		this.weight = weight;
		this.maxSize = 130;
	}

	draw (ctx) {
		let color;
		if (Array.isArray(this.color)) {
			color = ctx.createLinearGradient(0, 0, ctx.canvas.width / 2, 0);
			color.addColorStop(0, this.color[0]);
			color.addColorStop(1, this.color[1]);
		} else {
			color = this.color;
		}

		ctx.beginPath();
		ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2, false)
		ctx.fillStyle = color;
		ctx.closePath();
		ctx.fill();
	}

	update (ctx, canvas) {
		if (this.size < this.maxSize) {
			this.size += 2;
			this.draw(ctx);
		}
	}
}