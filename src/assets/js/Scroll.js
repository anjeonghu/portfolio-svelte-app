export class Scroll {
	constructor (ele, root) {
		this.ele = ele;
		this.scrollHeight;
		this.sectionOffsetTop;
		this.sectionScrollTop;
		this.scrollRealHeight;
		this.winScrollTop;
		this.scrollPercent;
		this.percent;
		this.lastScrollTop = 0;
		this.isDirDown = false;
		this.root = root;
	}
	initScroll () {
		this.stageHeight = this.root ? this.root.offsetHeight : window.innerHeight;

		this.scrollHeight = this.ele.offsetHeight; // 스크롤 높이
		this.sectionOffsetTop = this.ele.offsetTop; //섹션의 오프셋 탑 구함

		this.scrollRealHeight = (this.scrollHeight - this.stageHeight); //실제로 스크롤해야될 높이값을 구합니다
		// this.winScrollTop = window.scrollY  || document.documentElement.scrollTop; //스크롤바의 현재 위치를 구합니다
		this.winScrollTop = this.root ? this.root.scrollTop : window.scrollY; //스크롤바의 현재 위치를 구합니다
		this.sectionScrollTop = this.winScrollTop - this.sectionOffsetTop // 섹션의 탑 값을 구함

		this.scrollPerecnt =  (this.sectionScrollTop / this.scrollRealHeight); // 스크롤탑 / 스크롤 길이 로 비율을 구합니다
		this.percent = this.scrollPerecnt * 100

		this.isDirDown = this.winScrollTop > this.lastScrollTop;
		this.lastScrollTop = this.winScrollTop <= 0 ? 0 : this.winScrollTop; // For Mobile or negative scrolling
	}
}