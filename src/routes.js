import Home from './pages/Home.svelte'
import Blog from './pages/Blog.svelte'
import History from './pages/History/History.svelte'
import Publications from './pages/Publications/Publications.svelte'
import Interactions from './pages/Interactions/Interactions.svelte'
import AboutMe from './pages/AboutMe/AboutMe.svelte'

const routes = {
	'/': Home,
	'/history': History,
	'/publications': Publications,
	'/interactions': Interactions,
	'/aboutme': AboutMe,
}

export default routes
