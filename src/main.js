import './assets/scss/main.scss';
import App from './App.svelte';


function isNotMicrosoftEdgeBrowser() {
	return (navigator.userAgent.indexOf('chrome') !== -1) || (navigator.userAgent.indexOf('edg') !== -1);

}

if (isNotMicrosoftEdgeBrowser()){
	alert('Chrome 또는 최신 Edge 브라우저에서 실행해 주세요!!!!')
}

const app = new App({
	target: document.body,
	props: {
		name: 'world'
	}
});

export default app;