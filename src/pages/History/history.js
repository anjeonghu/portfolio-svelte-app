import {Scroll} from '../../assets/js/Scroll'

export class History  extends Scroll{
	constructor(root, container) {
		super(container, root);

		root.addEventListener('resize', this.resize.bind(this), false);
		root.addEventListener('scroll', this.scroll.bind(this), false);
		this.resize ();
	}

	resize () {
		this.initScroll();
		this.motionRender();
	}

	scroll () {
		this.initScroll();
		this.motionRender();
	}

	motionRender () {
		if(this.onScroll) {
			this.onScroll(this);
		}
	}
}
