const data =
	{
		MIRICANVAS: [
			[
				{
					"title": "미리캔버스",
					"subTitle": "미리캔버스 코어 엔진 개발",
					"writer": "jeongHu An",
					"copyright": "MIRICANVAS",
					"content1": ``,
					"content2":
						`<h1></h1>
							<p>
							&#183; 미리캔버스 코어 엔진 개발<br> 
							&#183; 미리 캔버스 텍스트 3.0 설계 및 개발<br> 
							&#183; 미리 캔버스 성능 최적화<br> 
							&#183; 미리 캔버스 3.0 모듈 설계<br> 
							&#183; 미리 캔버스 turbo repo를 이용한 모노레포 도입 및 Micro Frontend 적용<br> 
							</p>` +
						"<h1>사용 기술</h1>" +
						" <p class='projectskills'>" +
						"     <span class='label label-default skills'> Typescript </span>" +
						"     <span class='label label-default skills'> WebGL </span>" +
						"     <span class='label label-default skills'> BabylonJS </span>" +
						" </p>",
					"imgUrls": [
					],
					"position": "left",
					"colors": "#edf629, #c7c031, #9ea33d"
				},
			],
		],
		PLASK: [
			[
				{
					"title": "PLASK",
					"subTitle": "Plask Editing Animation Tool Engin<br> based BabylonJS",
					"writer": "jeongHu An",
					"copyright": "PLASK",
					"content1": ``,
					"content2":
						`<h1>Plask Editing Animation Tool Engin based BabylonJS</h1>
							<p>
							&#183; JavaScript 3D library중 하나인 Babylon.js의 Maintainer, Benjamin Guignabert<br> 
							와 BabylonJS 기반으로 PlaskEngine모듈 개발  <br> 
							</p>` +
						"<h1>사용 기술</h1>" +
						" <p class='projectskills'>" +
						"     <span class='label label-default skills'> Typescript </span>" +
						"     <span class='label label-default skills'> WebGL </span>" +
						"     <span class='label label-default skills'> BabylonJS </span>" +
						" </p>",
					"imgUrls": [
					],
					"position": "left",
					"colors": "#0acf83, #00c56c, #168d58"
				},
				{
					"title": "PLASK",
					"subTitle": "Plask Editing Animation Tool <br>Next Generation",
					"writer": "jeongHu An",
					"copyright": "PLASK",
					"content1": `<h1>Plask Editing Animation Tool Next Generation</h1>
							<p>
							&#183; Plask Editing Tool 고도화 프로젝트 개발<br> 
							</p>` +
						"<h1>사용 기술</h1>" +
						" <p class='projectskills'>" +
						"     <span class='label label-default skills'> Typescript </span>" +
						"     <span class='label label-default skills'> WebGL </span>" +
						"     <span class='label label-default skills'> BabylonJS </span>" +
						"     <span class='label label-default skills'> NextJS </span>" +
						"     <span class='label label-default skills'> React-Query </span>" +
						"     <span class='label label-default skills'> Recoil </span>" +
						" </p>",
					"content2": ``,
					"imgUrls": [
					],
					"position": "left",
					"colors": "#2f2f2f, #353434, #0e0c0c"
				},
			],
		],
		MAXST:
			[
				[
					{
						"title": "MAXST",
						"subTitle": "삼성 반도체 3D 저작도구",
						"writer": "jeongHu An",
						"copyright": "2021 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/삼성저작도구1.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>삼성 반도체 8계통 설비점검 3D 저작도구</h1>
							<p>
							&#183; 웹으로 설비점검 대상 장비 3D 컨텐츠 에  <br> 
							8계통 으로 분류된 점검 시트를 붙이고 위치을<br>
							조정하여 현장에서 AR 증강하여 사용 <br> 
							&#183; Multi Scene 기능 개발
							&#183; 이동, 회전, 크기 조정및 애니메이션 기능 구현
							&#183; Mouse Ray casting 구현
							&#183; Camera 회전 및 줌 기능 구현  
							&#183; 3D 설비점검 컨텐츠 Nomal vecotor를 이용한 점검부위 3d 컨텐츠 Sticky 기능 구현  
							&#183; 3D 저작 와 2D저작 도구와 연동 기능 구현 
							</p>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> WebGL </span>" +
							"     <span class='label label-default skills'> ThreeJS </span>" +
							"     <span class='label label-default skills'> CSS </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/삼성저작도구1.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/삼성저작도구4.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/삼성저작도구2.png",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/삼성저작도구3.png",
						],
						"position": "left",
						"colors": "#10d7f3, #08a6bc, #08a4ba"
					},
					{
						"title": "MAXST",
						"subTitle": "삼성 반도체 설비 점검 CMS",
						"writer": "jeongHu An",
						"copyright": "2021 MAXST",
						"content1":`<h1>삼성 반도체 설비 점검 CMS</h1>
							<p>
							&#183; 설비 점검 체크 시트 등록 <br> 
							&#183; 설비 등록<br>
							&#183; 설비 점검 크로스 체크 <br> 
							&#183; 설비 점검 2D 저작도구 제공 <br> 
							&#183; 점검 히스토리 제공 <br> 
							&#183; 점검 Report 통계 제공 <br> 
							&#183; 사용자 만명이상 되는 시스템 <br> 
							</p>`,
						"content2":
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> Vuex </span>" +
							"     <span class='label label-default skills'> HTML5 Canvas </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> SASS </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
						],
						"position": "right",
						"colors": "#7f48df, #6b3cbe, #553095"
					},
				],
				[
					{
						"title": "MAXST",
						"subTitle": "MAXWORK 판매사이트",
						"writer": "jeongHu An",
						"copyright": "2020 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/maxwork판매.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>Remote AR Support Service 소개및 판매 사이트</h1>
							<p>
							&#183; 모바일 반응형 판매 사이트 개발 <br> 
							&#183; 구매 결제 모듈 적용 및 UI/UX 개발 <br>
							&#183; NuxtJS를 이용한 SSR 렌더링 개발 <br> 
							&#183;  Sass의 Mixin, Include, 변수 등의 기능을 이용하여 설계 및 구현 <br> 
							</p>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> Vuex </span>" +
							"     <span class='label label-default skills'> WebRTC </span>" +
							"     <span class='label label-default skills'> WebGL </span>" +
							"     <span class='label label-default skills'> ThreeJS </span>" +
							"     <span class='label label-default skills'> Service Worker Notification API </span>" +
							"     <span class='label label-default skills'> HTML5 Canvas </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> SASS </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/maxwork판매.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/maxwork판매2.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/maxwork판매3.PNG",
						],
						"position": "left",
						"colors": "#2f2f2f, #353434, #0e0c0c"
					},
					{
						"title": "MAXST",
						"subTitle": "MAXWORK",
						"writer": "jeongHu An",
						"copyright": "2020 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/remote-contents-1@2x.png' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>Remote AR Support Service 개발 <br> 1:N 다자간 웹 영상 통화 및 AR 원격 지원 서비스</h1>
							<p>
							&#183;  Mutiple PeerConnection 지원을 위한 System Resource(Camera, MIC) Proxy 미들웨어 개발 <br> 
							&#183;  Media serverless로 구성 (full mesh network으로 구성)<br>
							&#183;  AR Drawing & AR 증강 및 AR Drawing UI/UX 개발 <br>
							&#183;  Sass의 Mixin, Include, 변수 등의 기능을 이용하여 설계 및 구현 <br>
							&#183;  Jest를 이용하여 API/Store Module 테스트 <br>
							</p>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> Vuex </span>" +
							"     <span class='label label-default skills'> WebRTC </span>" +
							"     <span class='label label-default skills'> WebGL </span>" +
							"     <span class='label label-default skills'> ThreeJS </span>" +
							"     <span class='label label-default skills'> Service Worker Notification API </span>" +
							"     <span class='label label-default skills'> HTML5 Canvas </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> SASS </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/remote-contents-1@2x.png",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/remote2.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/remote3.PNG",
						],
						"position": "right",
						"colors": "#0acf83, #00c56c, #168d58"
					},
				],
				[
					{
						"title": "MAXST",
						"subTitle": "삼성 ARGo 원격지원",
						"writer": "jeongHu An",
						"copyright": "2019 MAXST",
						"content1":
							`<h1>삼성 반도체 산업 현장 원격지원 웹앱</h1>
							<p>
							&#183;  웹 영상 통화 및 AR 원격 지원 서비스 <br>
							&#183;  P2P Media serverless Signaling Javascript SDK 개발<br>
							&#183;  AR Drawing & AR Object 증강 및 AR Drawing UI/UX 개발 <br>
							&#183;  Sass의 Mixin, Include, 변수 등의 기능을 이용하여 설계 및 구현 <br> 
							</p>`
						,
						"content2":
							`<h1>증강현실 컨텐츠 2D 저작도구 개발</h1>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> Vuex </span>" +
							"     <span class='label label-default skills'> WebRTC </span>" +
							"     <span class='label label-default skills'> WebGL </span>" +
							"     <span class='label label-default skills'> ThreeJS </span>" +
							"     <span class='label label-default skills'> HTML5 Canvas </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> SASS </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D저작도구.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D저작도구2.PNG",
						],
						"position": "left",
						"colors": "#e91e63, #ca1d58, #ad194b"
					},
					{
						"title": "MAXST",
						"subTitle": "현대 AR 가이드 CMS",
						"writer": "jeongHu An",
						"copyright": "2018 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/현대CMS.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>현대 AR 가이드 CMS 개발</h1>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> AngularJS </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> Html5 </span>" +
							"     <span class='label label-default skills'> Html5 Canvas </span>" +
							"     <span class='label label-default skills'> Sass </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/현대CMS.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/현대CMS2.PNG",
						],
						"position": "right",
						"colors": "#ff8e7f, #ff7335, #c05d32"
					},
				],
				[
					{
						"title": "MAXST",
						"subTitle": "증강현실 컨텐츠 2D 저작도구",
						"writer": "jeongHu An",
						"copyright": "2017 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D저작도구.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>증강현실 컨텐츠 2D 저작도구 개발</h1>` +
							`<p>
								&#183;  HTML5 Canvas를 이용한 스크린 캡처 기능 개발  <br>
								&#183;  Javascript UI 모듈 개발  <br>                                         
							</p>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> ReactJS </span>" +
							"     <span class='label label-default skills'> Redux </span>" +
							"     <span class='label label-default skills'> Html5 </span>" +
							"     <span class='label label-default skills'> Sass </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D저작도구.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D저작도구2.PNG",
						],
						"position": "left",
						"colors": "#edf629, #c7c031, #9ea33d"
					},
					{
						"title": "MAXST",
						"subTitle": "맥스트 홈페이지",
						"writer": "jeongHu An",
						"copyright": "2017 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/maxst홈페이지.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>맥스트 홈페이지 개발</h1>` +
							`<p>
								&#183;  모바일 반응형 홈페이지 개발 <br>
								&#183;  Sass Mixin, function 기능을 이용한 퍼블리싱 설계 및 개발 <br>
								&#183;  Greensock 을 이용한 애니메이션 모듈 개발
							</p>` +
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> GreenSock </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> ES6 </span>" +
							"     <span class='label label-default skills'> Html5 </span>" +
							"     <span class='label label-default skills'> Sass </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/maxst홈페이지.PNG",
						],
						"position": "right",
						"colors": "#0acf83, #00c56c, #168d58"
					},
				],
				[
					{
						"title": "MAXST",
						"subTitle": "맥스트 디벨로퍼 사이트",
						"writer": "jeongHu An",
						"copyright": "2016 MAXST",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/developer사이트1.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>맥스트 디벨로퍼 사이트</h1>
								<p>
								&#183; 맥스트 SDK 판매 및 개발자 가이드 사이트<br>
								&#183; Markdown을 활용해 동적 페이지 생성 및 메뉴 개발<br>
								&#183; RequireJS를 이용한 자바스크립트 모듈화<br>
								&#183; ASP.NET Razor 사용경험<br>
							</p>`+
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> ASP.net </span>" +
							"     <span class='label label-default skills'> Html5 </span>" +
							"     <span class='label label-default skills'> CSS </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> RequireJS</span>" +
							" </p>",
						"imgUrls": [
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/developer사이트1.PNG',
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/developer사이트2.PNG',
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/developer사이트2NG.PNG',
						],
						"position": "left",
						"colors": "#0c6cfd, #3f5ce3, #4f4dc9"
					}
				],
			],

		ETC:
			[
				[
					{
						"title": "ETC",
						"subTitle": "Brain Trainer",
						"writer": "jeongHu An",
						"copyright": "2016",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/brainTrainer2.jpg' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>두뇌 훈련용 하이브리드 게임</h1>
								<p>
								&#183; 암산 관 암기력을 함께 훈련할 수 있는 게임<br>
								&#183; 한때 1000여명 다운로드 - 현재 서버 문제로 구글 앱스토어에서 내림 
							</p>`+
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> Html5 </span>" +
							"     <span class='label label-default skills'> SASS </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/brainTrainer4.jpg',
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/brainTrainer3.jpg',
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/brainTrainer2.jpg',
							'https://raw.githubusercontent.com/happenask/staticStorage/master/images/brainTrainer.jpg'
						],
						"position": "left",
						"colors": "#0c6cfd, #3f5ce3, #4f4dc9"
					},
					{
						"title": "ETC",
						"subTitle": "Tombolo 홈페이지",
						"writer": "jeongHu An",
						"copyright": "2018",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/tombolo.PNG' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>Tombolo 홈페이지</h1>
								<p>
								&#183; 주식회사 Tombolo 홈페이지 외주 제작<br>
								&#183; <a href="http://tombolo.com">Site URL → http://tombolo.com</a>
							</p>`+
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> VueJS </span>" +
							"     <span class='label label-default skills'> Html5 </span>" +
							"     <span class='label label-default skills'> SASS </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/tombolo.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/tombolo3.PNG",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/tombolo2.PNG"
						],
						"position": "right",
						"colors": "#edf629, #c7c031, #9ea33d"
					},
				],
				[
					{
						"title": "ETC",
						"subTitle": "Kone 홈페이지",
						"writer": "jeongHu An",
						"copyright": "2019",
						"content1": `<img src='https://raw.githubusercontent.com/happenask/staticStorage/master/images/kone_1.png' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>Kone 홈페이지 제작</h1>
								<p>
								&#183; 주식회사 Kone 홈페이지 외주 제작<br>
							</p>`,
						"imgUrls": [
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/kone_1.png",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/kone_2.png",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/kone_3.png",
							"https://raw.githubusercontent.com/happenask/staticStorage/master/images/kone_4.png"
						],
						"position": "left",
						"colors": "#0acf83, #00c56c, #168d58"
					},
					{
						"title": "ETC",
						"subTitle": "Boxing Trainer",
						"writer": "jeongHu An",
						"copyright": "2020",
						"content1": `<img src='https://github.com/happenask/staticStorage/blob/master/images/boxingTrain.PNG?raw=true' alt='' style="cursor: pointer"/>`,
						"content2":
							`<h1>Boxing Trainer</h1>
								<p>
								&#183; 화면에 보이는 얼굴을 인식해서 여러방향에서 무작위로 날라오는 주먹을 피하면 점수를 얻는 게임<br>
							</p>`+
							"<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> Face Api </span>" +
							"     <span class='label label-default skills'> Web Media Api </span>" +
							"     <span class='label label-default skills'> Tensorflow </span>" +
							"     <span class='label label-default skills'> CSS </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> Webpack</span>" +
							" </p>",
						"imgUrls": [
							"https://github.com/happenask/staticStorage/blob/master/images/boxingTrain.PNG?raw=true"
						],
						"position": "right",
						"colors": "#ff8e7f, #ff7335, #c05d32"
					},
				]
			],
		GONG8LITER: [
			[
				{
					"title": ".8L",
					"subTitle": "공팔리터 소셜 플랫폼",
					"writer": "jeongHu An",
					"copyright": "2016 .8L",
					"content1": `<img src='https://github.com/happenask/staticStorage/blob/master/images/공팔리터.PNG?raw=true' alt='' style="cursor: pointer"/>`,
					"content2":
						`<h1>공팔리터 소셜 플랫폼 웹사이트 개발</h1>
							<p>
							&#183; Angular js 프레임 워크 기반 SPA 반응형 판매 사이트 개발<br>
							&#183; Spring Framwork 기반 관리자 페이지 개발
							</p>` +
						"<h1>사용 기술</h1>" +
						" <p class='projectskills'>" +
						"     <span class='label label-default skills'> AngularJS </span>" +
						"     <span class='label label-default skills'> Html5 </span>" +
						"     <span class='label label-default skills'> SASS </span>" +
						"     <span class='label label-default skills'> CSS </span>" +
						"     <span class='label label-default skills'> Javascript </span>" +
						"     <span class='label label-default skills'> Gulp</span>" +
						"     <span class='label label-default skills'> Spring</span>" +
						"     <span class='label label-default skills'> JSP</span>" +
						" </p>",
					"imgUrls": [
						"https://github.com/happenask/staticStorage/blob/master/images/공팔리터.PNG?raw=true",
						"https://github.com/happenask/staticStorage/blob/master/images/공팔리터2.PNG?raw=true",
						"https://github.com/happenask/staticStorage/blob/master/images/공팔리터3.PNG?raw=true"
					],
					"position": "left",
					"colors": "#ff8e7f, #ff7335, #c05d32"
				},
			]
		],
		UNITAS:
			[
				[
					{
						"title": "UNITAS",
						"subTitle": "교촌, 굽네, 페리카나 치킨 수발주 시스템",
						"writer": "jeongHu An",
						"copyright": "2015 UNITAS",
						"content1":
							`<h1>교촌치킨, 굽네치킨, 페리카나 치킨 수발주 시스템 개발</h1>
							<p>
							&#183; 가맹주들이 간편하게 웹으로 수발주 기능 및 재고 관리, 매출 통계 기능 등을 사용 할수 있고, <br> 물류 창고에서는 거래 명세서 발급및 다양한 수발주 기능을 사용할 수 있는 웹 어플리케이션
							&#183; Spring Framwork 기반 수발주 시스템 백엔드, 프론트 개발
							</p>`
						,
						"content2": "<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> Spring </span>" +
							"     <span class='label label-default skills'> MyBatis </span>" +
							"     <span class='label label-default skills'> MSSQL </span>" +
							"     <span class='label label-default skills'> JSP </span>" +
							"     <span class='label label-default skills'> Jquery </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> CSS</span>" +
							"     <span class='label label-default skills'> Tomcat</span>" +
							" </p>",
						"imgUrls": [
						],
						"position": "left",
						"colors": "#0c6cfd, #3f5ce3, #4f4dc9"
					},
					{
						"title": "UNITAS",
						"subTitle": "피자알볼로, 스쿨푸드 - 매출분석 모바일웹",
						"writer": "jeongHu An",
						"copyright": "2014 UNITAS",
						"content1":
							`<h1>피자알볼로, 스쿨푸드 - 매출분석 모바일웹 개발</h1>
							<p>
							&#183; 매출분석 모바일웹 백엔드, 프론트 개발<br>
							</p>`
						,
						"content2": "<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> Spring </span>" +
							"     <span class='label label-default skills'> MyBatis </span>" +
							"     <span class='label label-default skills'> MSSQL </span>" +
							"     <span class='label label-default skills'> JSP </span>" +
							"     <span class='label label-default skills'> Jquery </span>" +
							"     <span class='label label-default skills'> PhonGap </span>" +
							"     <span class='label label-default skills'> Javascript </span>" +
							"     <span class='label label-default skills'> CSS</span>" +
							"     <span class='label label-default skills'> Tomcat</span>" +
							" </p>",
						"imgUrls": [
						],
						"position": "right",
						"colors": "#edf629, #c7c031, #9ea33d"
					},
					{
						"title": "UNITAS",
						"subTitle": "IReport 거래명세서 레포팅 프로그램",
						"writer": "jeongHu An",
						"copyright": "2014 UNITAS",
						"content1":
							`<h1>IReport 거래명세서 레포팅 프로그램 개발</h1>
							<p>
							&#183; 기존 수동으로 발행했던 거래명세서를 웹으로 손쉽게 발행<br>
							&#183; 오픈 소스를 이용한 IReport 거래명세서 레포팅 프로그램 개발<br>
							</p>`
						,
						"content2": "<h1>사용 기술</h1>" +
							" <p class='projectskills'>" +
							"     <span class='label label-default skills'> Spring </span>" +
							"     <span class='label label-default skills'> MyBatis </span>" +
							"     <span class='label label-default skills'> MSSQL </span>" +
							"     <span class='label label-default skills'> JSP </span>" +
							"     <span class='label label-default skills'> Jasper Report </span>" +
							"     <span class='label label-default skills'> IReport </span>" +
							" </p>",
						"imgUrls": [
						],
						"position": "left",
						"colors": "#0acf83, #00c56c, #168d58"
					},
				]
			]
	}


export default data
