import {FBXLoader} from "../assets/lib/FBXLoader.js";
import * as THREE from '../assets/lib/three.module.js';

export default class Player{
	constructor(park){
		this.park = park;
		this.name = 'BusinessMan';
		this.assetsPath = '../assets/';
		this.object = null;
		this.animations = {};
		this.mixer = null;
		this.actionName = null;
		this.actionTime = null;
		
		this.init();

	}
	init () {
		const loader = new FBXLoader();
		let _self =  this;
		loader.load( `${this.assetsPath}FBX/${_self.name}.fbx`, function ( object ) {

			object.mixer = new THREE.AnimationMixer( object );
			_self.mixer = object.mixer;

			//_self.name = "Person";

			object.traverse( function ( child ) {
				if ( child.isMesh ) {
					child.castShadow = false;
					child.receiveShadow = false;
				}
			} );
			object.visible = true;


			const textureLoader = new THREE.TextureLoader();

			textureLoader.load(`${_self.assetsPath}Textures/SimplePeople_BusinessMan_White.png`, function(texture){
				object.traverse( function ( child ) {
					if ( child.isMesh ){
						child.material.map = texture;
					}
				} );
			});

			_self.object = new THREE.Object3D();
			_self.animations.Idle = object.animations[0];

			_self.park.add(_self.object);
			_self.object.rotateY(Math.PI / 2);
			// _self.object.position.set(0, 0, 0);
			_self.object.add(object);


			loader.load( `${_self.assetsPath}FBX/Running.fbx`, function( object ){
				_self.animations.Running = object.animations[0];
			});

			_self.action = 'Idle';
		} );

	}
	set action(name){
		if(this.animations[name] === undefined) return;

		const clip = THREE.AnimationClip.parse(THREE.AnimationClip.toJSON(this.animations[name]));
		const action = this.mixer.clipAction( clip );
		action.time = 0;
		this.mixer.stopAllAction();
		this.actionName = name;
		this.actionTime = Date.now();

		action.fadeIn(0.5);
		action.play();
	}

	get action(){
		return this.actionName;
	}

	update(dt){
		if(this.mixer) {
			this.mixer.update(dt);
		}
	}
}