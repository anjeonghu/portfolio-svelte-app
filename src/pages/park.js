import * as THREE from '../assets/lib/three.module.js';
import {GLTFLoader} from "../assets/lib/GLTFLoader.js";
import {OrbitControls} from '../assets/lib/OrbitControls.js';
import {JoyStick} from '../assets/lib/toon3d.js'
import Player from "./Player.js";
import PlayerController from "./PlayerController.js";
import SpeechBubble from "./SpeechBubble.js";

// import { RectAreaLightHelper } from './libs/RectAreaLightHelper.js';

export class Park {
	constructor() {
		this.container = null;
		this.player = null;
		this.playerController = null;
		this.cameras = {};
		this.cameraMode = 'init';
		this.cameraActive = false;
		this.playerActive = true;
		this.camera = null;
		this.scene = null;
		this.renderer = null;
		this.animations = {};
		this.assetsPath = 'assets/';
		this.colliders = [];
		this.spots = ['Spot1', 'Spot2', 'Spot3', 'Spot4'];
		this.onPlayerOnSpot = () => {};
		this.joystick = null;
		this.speechBubble = null;
		this.clock = new THREE.Clock();
		this.container = document.createElement( 'div' );
		this.container.style.height = '100%';

		this.loadingManager = new THREE.LoadingManager();
		this.loader = new GLTFLoader(this.loadingManager);
		this.loader2 = new GLTFLoader();

		document.querySelector('.home').appendChild( this.container );

		if ('ontouchstart' in window){
			window.addEventListener( 'touchdown', (event) => this.onMouseDown(event), false );
		}else{
			window.addEventListener( 'mousedown', (event) => this.onMouseDown(event), false );
		}

		window.addEventListener('resize', this.resize.bind(this), false);

		// initialize park
		this.init().then(r =>{})
	}
	resize () {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();

		this.renderer.setSize( window.innerWidth, window.innerHeight );
	}
	async init () {
		// camera
		this.camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 10, 20000 );
		this.camera.position.set( -2600, 6000, 5000);

		// camera positions
		const front = new THREE.Object3D();
		front.position.set(850, 550, 0);
		const top = new THREE.Object3D();
		top.position.set(2500, 2200, 0);

		this.cameras.front = front;
		this.cameras.top = top;
		this.cameraMode = 'init';
		this.cameraActive = false;

		// scene
		this.scene = new THREE.Scene();

		const light1 = new THREE.PointLight( 0xffffff, 3, 0 );
		light1.position.set( 420, 200, 0 );
		this.scene.add(light1);
		let light2 = new THREE.HemisphereLight( 0xffffff, 0x000000); //
		light2.intensity = 1;
		light2.position.set( 0, 10, 0 );
		this.scene.add( light2 );

		await this.loadEnvironment();

		this.player = new Player(this.scene);
		this.playerController = new PlayerController(this.player, this.scene);
		this.playerController.setColliders(this.colliders);

		// playerController Events
		this.playerController.onPlayerOnSpot = (spotName) => {
			if (this.spots.includes(spotName)) {
				this.onPlayerOnSpot(spotName);
			}
		}

		// const axesHelper = new THREE.AxesHelper( 50 );
		// this.scene.add( axesHelper );

		this.renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
		this.renderer.setPixelRatio( window.devicePixelRatio );
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		this.renderer.toneMapping = THREE.ReinhardToneMapping;
		this.renderer.toneMappingExposure = 2.3;
		this.renderer.shadowMap.enabled = true;
		this.renderer.shadowMap.type = THREE.BasicShadowMap;
		this.container.appendChild( this.renderer.domElement );

		this.controls = new OrbitControls(this.camera, this.renderer.domElement);
		// this.controls.minDistance = 2;
		// this.controls.maxDistance = 10;
		this.controls.target.set(0, 0, 0);
		this.controls.update();
		this.animate();

		await this.loadEnvironment2();
	}
	loadEnvironment(){
		const _self = this;
		return new Promise ((resolve) => {
			this.loader.load(`${this.assetsPath}FBX/scene1.gltf`, (gltf) => {
				_self.environment = gltf.scene;
				_self.colliders = [];
				_self.scene.add(gltf.scene);
				_self.camera.lookAt(gltf.scene.position)
				gltf.scene.traverse( function ( child ) {
					if ( child.isMesh ) {
						_self.colliders.push(child);
						child.castShadow = true;
						child.receiveShadow = true;
					}
				} );

				resolve();
			})
		});

	};
	loadEnvironment2(){
		const _self = this;
		return new Promise ((resolve) => {
			this.loader2.load(`${this.assetsPath}FBX/scene2.gltf`, (gltf) => {
				_self.environment2 = gltf.scene;
				_self.scene.add(gltf.scene);
				gltf.scene.traverse( function ( child ) {
					if ( child.isMesh ) {
						child.castShadow = true;
						child.receiveShadow = true;
					}
				} );

				resolve();
			})
		});

	}
	onMouseDown () {

	}
	setJoyStick () {
		console.log('setjoystick')
		this.joystick = new JoyStick({
			onMove: this.playerController.playerControl.bind(this.playerController),
			game: this
		});
	}
	setSpeechBubble () {
		const msg = '안녕하세요 안정후 공원에 오신걸 환영합니다. 아래 [NEXT]를 클릭한후, 조이스틱을 조작해 공원을 둘러보면서 저의 작품들을 감상해 보세요.'
		this.speechBubble = new SpeechBubble(this.scene, msg, 300);
		this.speechBubble.setPlayer(this.player);
	}
	removeSpeechBubble () {
		this.speechBubble.hide();
	}
	set activePlayer (active) {
		this.playerActive = active;
	}
	set activeCameraMode (mode) {
		if (mode === 'front') {
			this.cameraMode = mode;
			this.cameraActive = true
			this.setSpeechBubble();
		} else if (mode === 'top') {
			this.cameraMode = mode;
			this.cameraActive = true
			this.removeSpeechBubble();
			this.setJoyStick();
		} else {
			this.cameraActive = false;
		}
	}
	animate() {
		const dt = this.clock.getDelta();

		requestAnimationFrame(this.animate.bind(this));

		if (this.playerController.player.object) {
			if (this.playerActive) {
				this.playerController.move(dt);
			}

			if (this.speechBubble && this.cameraMode === 'front') {
				this.speechBubble.show(this.camera.position);
			}
		}

		if (this.camera!==undefined && this.player!==undefined && this.player.object!==undefined){
			if (this.cameraActive) {
				if (this.cameraMode === 'front') {
					this.camera.position.lerp(this.cameras.front.getWorldPosition(new THREE.Vector3()), 0.05);
					const pos = this.player.object.position.clone();
					pos.y += 300;
					this.camera.lookAt(pos);
				} else if (this.cameraMode === 'top') {
					this.camera.position.lerp(this.cameras.top.getWorldPosition(new THREE.Vector3()), 0.05);
					this.camera.lookAt(new THREE.Vector3(0,0,0));
				}
			}

		}

		this.renderer.render( this.scene, this.camera );

	}
}
