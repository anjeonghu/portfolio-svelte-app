import * as THREE from '../assets/lib/three.module.js';

export default class SpeechBubble {
	constructor(scene, msg, size) {
		this.player = null;
		this.scene = scene;
		this.mesh = null;
		this.img = null;
		this.width = 300;
		this.height = 250;
		this.size = size;
		this.assetsPath = 'assets/';
		this.padding = 20;

		const planeGeometry = new THREE.PlaneGeometry(this.size, this.size);
		const planeMaterial = new THREE.MeshStandardMaterial();
		this.mesh = new THREE.Mesh(planeGeometry, planeMaterial);
		this.canvas = null;
		this.scene.add(this.mesh);

		const self = this;

		const loader = new THREE.TextureLoader();
		loader.load(
			// resource URL
			`${this.assetsPath}Textures/speech.png`,

			// onLoad callback
			function ( texture ) {
				// in this example we create the material when the texture is loaded
				self.img = texture.image;
				self.mesh.material.map = texture;
				self.mesh.material.transparent = true;
				self.mesh.material.needsUpdate = true;
				self.mesh.visible = false;
				if (msg!==undefined) self.update(msg);
			},

			// onProgress callback currently not supported
			undefined,

			// onError callback
			function ( err ) {
				console.error( 'An error happened.' );
			}
		);
	}

	setPlayer(player) {
		this.player = player;
	}

	update (msg) {
		if (this.mesh===undefined) return
		this.canvas = this.createOffscreenCanvas(this.width, this.height);
		this.ctx = this.canvas.getContext('2d');
		this.ctx.drawImage(this.img, 0, 0, this.img.width, this.img.height, 0, 0, this.width, this.height);

		this.ctx.font = "bold 18px Arial";
		this.ctx.fillStyle = 'rgb(0, 0, 0)';
		this.ctx.textAlign = 'left';

		this.wrapText(msg, this.ctx);
		this.mesh.material.map = new THREE.Texture(this.canvas);

		this.mesh.material.map.needsUpdate = true;
	}

	createOffscreenCanvas(w, h) {
		const canvas = document.createElement('canvas');
		canvas.width = w;
		canvas.height = h;
		return canvas;
	}

	wrapText(text, context){
		const words = text.split(' ');
		let line = '';
		const lines = [];
		const maxWidth = this.width - 2 * this.padding;
		const lineHeight = 25;

		words.forEach( function(word){
			const testLine = `${line}${word} `;
			const metrics = context.measureText(testLine);
			const testWidth = metrics.width;
			if (testWidth > maxWidth) {
				lines.push(line);
				line = `${word} `;
			}else {
				line = testLine;
			}
		});

		if (line !== '') lines.push(line);

		let y = (this.height - lines.length * lineHeight)/2;

		lines.forEach( (line) => {
			context.fillText(line, this.padding, y);
			y += lineHeight;
		});
	}

	show(pos){
		if (this.mesh!==undefined && this.player!==undefined){
			this.mesh.position.set(this.player.object.position.x , this.player.object.position.y + 450, this.player.object.position.z - 20);
			this.mesh.lookAt(pos);
			this.mesh.visible = true;
		}
	}
	hide() {
		if (this.mesh!==undefined && this.player!==undefined) {
			this.mesh.visible = false;
		}
	}
}