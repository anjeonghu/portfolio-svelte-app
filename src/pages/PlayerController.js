import * as THREE from '../assets/lib/three.module.js';

export default class PlayerController {
	constructor(player,scene) {
		this.player = player;
		this.scene = scene;
		this.active = false;
		this.velocityY = 0;
		this.forward = 0;
		this.turn = 0;
		this.colliders = null;
		this.blocked = false;
		this.blocks = ['UnderGround', 'UnderGround_1', 'UnderGround_2', 'UnderGround_3', 'UnderGround_4', 'Water'];
		this.spots = ['Spot1', 'Spot2', 'Spot3', 'Spot4'];
		this.currentSpot = null;
		this.currentSpotName = '';
		this.onPlayerOnSpot = () => {};

		// const buffergeometry1 = new THREE.BufferGeometry();
		// buffergeometry1.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );
		// const buffergeometry2 = new THREE.BufferGeometry();
		// buffergeometry2.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );
		// const buffergeometry3 = new THREE.BufferGeometry();
		// buffergeometry3.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );
		// const buffergeometry4 = new THREE.BufferGeometry();
		// buffergeometry4.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );
		//
		// this.lineHelperFront = new THREE.Line( buffergeometry1, new THREE.LineBasicMaterial({color: 0x0000ff}) );
		// this.lineHelperFront.visible = true;
		// this.lineHelperDown = new THREE.Line( buffergeometry4, new THREE.LineBasicMaterial({color: 0x0000ff}) );
		// this.lineHelperDown.visible = true;
		// this.scene.add(this.lineHelperFront);
		// this.scene.add(this.lineHelperDown);
	}
	init () {

	}
	setColliders (colliders) {
		this.colliders = colliders;
	}
	handlePlayerOnSpot (obj) {
		console.log(obj.parent.name);

		if (this.spots.includes(obj.parent.name)) {
			if (obj.parent.name !== this.currentSpotName) {
				obj.material.emissiveIntensity = 5;
				this.currentSpotName = obj.parent.name;
				this.currentSpot = obj;
				this.onPlayerOnSpot.call(this, obj.parent.name);
			}
			return true;
		} else {
			if (this.currentSpot) {
				this.currentSpotName = '';
				this.currentSpot.material.emissiveIntensity = 2;
			}
		}
		return false;
	}
	playerControl(forward, turn){
		this.forward = forward;
		this.turn = turn;

		this.active = !(this.forward === 0 && this.turn === 0);

		if (this.active){
			if (this.player.action !== 'Running') this.player.action = 'Running';
		} else{
			if (this.player.action !== 'Idle'){
				this.player.action = 'Idle';
			}
		}
	}
	move(dt) {
		const speed = 600;
		this.player.update(dt);

		if (!this.active) return;

		const pos = this.player.object.position.clone();
		pos.y += 160;
		let dir = new THREE.Vector3();
		this.player.object.getWorldDirection(dir);

		// const positions = this.lineHelperFront.geometry.attributes.position;
		// positions.setXYZ(0, pos.x, pos.y, pos.z);
		// positions.setXYZ(1, dir.x * 10000, dir.y * 10000, dir.z * 10000);
		// positions.needsUpdate = true;

		let raycaster = new THREE.Raycaster(pos, dir);
		const colliders = this.colliders;

		if (colliders !== undefined) {
			const intersect = raycaster.intersectObjects(colliders);
			if (intersect.length > 0) {
				if (intersect[0].distance < 50) this.blocked = true;
			}
		}
		let r = new THREE.Vector3();
		r.set(this.forward * -1, 0, this.turn * -1);
		r.multiplyScalar(10);
		let v = new THREE.Vector3();
		v.addVectors(r, this.player.object.position);

		// r.normalize();
		this.player.object.lookAt(v);
		if (!this.blocked) {
			this.player.object.translateZ(dt * speed);
		}

		if (colliders !== undefined) {
			//cast left
			dir.set(-1, 0, 0);
			dir.applyMatrix4(this.player.object.matrix);
			dir.normalize();
			raycaster = new THREE.Raycaster(pos, dir);

			let intersect = raycaster.intersectObjects(colliders);
			if (intersect.length > 0) {
				if (intersect[0].distance < 50) this.player.object.translateX(100 - intersect[0].distance);
			}

			//cast right
			dir.set(1, 0, 0);
			dir.applyMatrix4(this.player.object.matrix);
			dir.normalize();
			raycaster = new THREE.Raycaster(pos, dir);

			intersect = raycaster.intersectObjects(colliders);
			if (intersect.length > 0) {
				if (intersect[0].distance < 50) this.player.object.translateX(intersect[0].distance - 100);
			}

			const posD = this.player.object.position.clone();
			//cast down
			dir.set(0, -1, 0);
			posD.y += 60;
			raycaster = new THREE.Raycaster(posD, dir);
			const gravity = 30;

			// const positions3 = this.lineHelperDown.geometry.attributes.position;
			// positions3.setXYZ(0, posD.x, posD.y, posD.z);
			// positions3.setXYZ(1, dir.x * 10000, dir.y * 10000, dir.z * 10000);
			// positions3.needsUpdate = true;

			intersect = raycaster.intersectObjects(colliders);
			if (intersect.length > 0) {
				if (this.blocks.includes(intersect[0].object.name)) {
					this.blocked = true;
					this.player.object.translateZ(-30);
					return;
				}
				this.handlePlayerOnSpot(intersect[0].object);
				this.blocked = false;

				const targetY = posD.y - intersect[0].distance;
				if (targetY > this.player.object.position.y) {
					//Going up
					this.player.object.position.y = 0.8 * this.player.object.position.y + 0.2 * targetY;
					this.velocityY = 0;
				} else if (targetY < this.player.object.position.y) {
					//Falling
					if (this.velocityY === undefined) this.velocityY = 0;
					this.velocityY += dt * gravity;
					this.player.object.position.y -= this.velocityY;
					if (this.player.object.position.y < targetY) {
						this.velocityY = 0;
						this.player.object.position.y = targetY;
					}
				}
			}

		}
	}
}