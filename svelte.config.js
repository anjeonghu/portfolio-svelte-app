const autoprefixer = require('autoprefixer');
const sveltePreprocess = require('svelte-preprocess');

const production = !process.env.ROLLUP_WATCH;

module.exports = {
	compilerOptions: {
		// enable run-time checks when not in production
		dev: !production
	},
	preprocess: sveltePreprocess({
		postcss: {
			plugins: [autoprefixer()]
		},
	}),
}